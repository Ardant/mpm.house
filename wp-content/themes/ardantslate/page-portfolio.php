<?php /* Template name: Portfolio */get_header(); ?>

</div><!--end container-->
<div id="slick" class="slick">
<?php $images = get_field('slideshow');
    if( $images ): ?>
		<?php foreach( $images as $image ): ?>
            <div>
	            <img src="<?php echo $image['sizes']['large']; ?>" alt=""/>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<style>
body {
	color: black;
xbackground: no-repeat right top fixed url(<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'fullsize' );echo $large_image_url[0];?>);
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
}
</style>
<div class="container">
<div class="row">
    <section id="content" role="main" class="col-xs-12 portfoliocontent">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="header">
    <h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
    </header>
    <section class="entry-content">
    <?php the_content(); ?>
    <div class="entry-links"><?php wp_link_pages(); ?></div>
    </section>
    </article>
    <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
    <?php endwhile; endif; ?>
    </section>
</div>
</div><!--end container-->
<?php 
if ( !is_page('redevelopment')) {;
$operator = "NOT IN";
} else {
$operator = "IN";	
}

$wpb_all_query = new WP_Query(array(
	'post_type'=>'property',
	'post_status'=>'publish',
	'taxonomy' => 'classco',
	'posts_per_page'=>-1,
	'order' => 'DESC', 
	'tax_query'=>array(
    	array(
            'taxonomy'=>'classco',
            'field'=>'slug',
            'terms'=>array('redevelopment'),
			'operator' => $operator
		)
	)

	)); 
if ( $wpb_all_query->have_posts() ) :
	while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
    <div class="fullimg" style="background-image:url(<?php echo get_field('property_image');?>);">
     </div>
    <div class="container">
    <div class="row" style="background-image:url()">
        <section class="col-xs-12 col-sm-6 property-text">
            <h1 class="entry-title pad-title"><?php the_title(); ?></h1>
            <?php the_content();?>
        </section>
        <section class="col-xs-12 col-sm-6 property-gallery">
			<?php $images = get_field('property_gallery');
            if( $images ): 
            foreach( $images as $image ): ?>
            <a href="<?php echo $image['url']; ?>">
            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            </a>
            <?php endforeach; ?>
            <?php endif; ?>	
		</section>
    </div>
    </div>
	<?php endwhile;
	wp_reset_postdata();
endif; ?>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
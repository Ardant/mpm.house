<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
<div class="container">
	&copy; <?php echo date("Y") ?> Morgan Property Management
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>
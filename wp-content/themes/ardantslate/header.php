<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/bootslate.css" />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/slick-theme.css">
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>?<?php echo rand(1,999999999999999); ?>" />
<link type="text/plain" rel="author" href="/humans.txt" />
<?php wp_head(); ?>
</head>
<body ontouchstart="" <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<header id="header" role="banner">
<section id="branding">
<div id="site-title"><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; } ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
<img src="<?php echo get_template_directory_uri()?>/img/MPM-House-Logo.png" alt="MPM House" width="320px"/>
</a><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?></div>
</section>
</header>
<nav id="menu" role="navigation">
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>
<div class="container main-container">
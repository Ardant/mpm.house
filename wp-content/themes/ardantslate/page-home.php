<?php /* Template name: Home Page*/ get_header(); ?>
</div>
<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'fullsize' );?>
<div class="fullimg homeimg" style="background-image:url(<?php echo $large_image_url[0];?>);">
</div>
<div class="container pad-bottom">
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header">
<h1 class="entry-title home-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<section class="entry-content home-content">
<?php the_content(); ?>
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php get_footer(); ?>
<?php
add_action( 'after_setup_theme', 'ardantslate_setup' );
function ardantslate_setup()
{
load_theme_textdomain( 'ardantslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'ardantslate' ) )
);
}
add_action( 'wp_enqueue_scripts', 'ardantslate_load_scripts' );
function ardantslate_load_scripts()
{
wp_enqueue_script( 'jquery' );
}

wp_register_script( 'il-js', '//unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js', array( 'jquery' ), '1', true );
wp_enqueue_script( 'il-js' );

wp_register_script( 'slick-js', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array( 'jquery' ), '1.6', true );
wp_enqueue_script( 'slick-js' );

wp_register_script( 'ardant-js', get_template_directory_uri() . '/js/ardant.js', array( 'jquery' ), '1.0.1', true );
wp_enqueue_script( 'ardant-js' );

add_action( 'comment_form_before', 'ardantslate_enqueue_comment_reply_script' );
function ardantslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'ardantslate_title' );
function ardantslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'ardantslate_filter_wp_title' );
function ardantslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'ardantslate_widgets_init' );
function ardantslate_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'ardantslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function ardantslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php 
}
add_filter( 'get_comments_number', 'ardantslate_comments_number' );
function ardantslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}
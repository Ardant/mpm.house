<?php /* Template name: About Page */get_header(); ?>
<style>
body {
background: no-repeat right top fixed url(<?php $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'fullsize' );echo $large_image_url[0];?>);
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
}
</style>
<div class="row">
    <section id="content" role="main" class="col-xs-12 col-sm-6 aboutcontent">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="header">
    <h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
    </header>
    <section class="entry-content">
    <?php the_content(); ?>
    <div class="entry-links"><?php wp_link_pages(); ?></div>
    </section>
    </article>
    <?php if ( ! post_password_required() ) comments_template( '', true ); ?>
    <?php endwhile; endif; ?>
    </section>
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>